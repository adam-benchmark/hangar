<?php

namespace Model;


use ArrayObject;

class SmallLocallyTrack
{
    const SMALL_LOCALLY_TRACK_MAX_WEIGHT = 200;
    const PREFIX_BARCODE_TRACK = 'TRACK';

    /** @var  ArrayObject */
    private $parcels;
    /** @var  integer */
    private $totalWeight;
    /** @var  string */
    private $trackBarcode;

    /**
     * SmallLocallyTrack constructor.
     */
    public function __construct()
    {
        $this->totalWeight = 0;
        $this->parcels = new ArrayObject();
        $this->setTrackBarcode($this->generateBarcodeForTrack());
    }

    /**
     * @return string
     */
    public function generateBarcodeForTrack()
    {
        return sprintf('%s%02s', self::PREFIX_BARCODE_TRACK, rand(1,10) );
    }

    /**
     * @param Parcel $parcel
     */
    public function addParcelAndIncreaseWeight(Parcel $parcel)
    {
        $this->parcels->append($parcel);
        $this->totalWeight += $parcel->getWeight();
    }

    /**
     * @return int
     */
    public function getTotalWeight(): int
    {
        return $this->totalWeight;
    }

    /**
     * @param int $totalWeight
     */
    public function setTotalWeight(int $totalWeight)
    {
        $this->totalWeight = $totalWeight;
    }

    /**
     * @return string
     */
    public function getTrackBarcode(): string
    {
        return $this->trackBarcode;
    }

    /**
     * @param string $trackBarcode
     */
    public function setTrackBarcode(string $trackBarcode)
    {
        $this->trackBarcode = $trackBarcode;
    }

    /**
     * @return ArrayObject
     */
    public function getParcels(): ArrayObject
    {
        return $this->parcels;
    }

    /**
     * @param ArrayObject $parcels
     */
    public function setParcels(ArrayObject $parcels)
    {
        $this->parcels = $parcels;
    }


}