<?php

namespace Model;

class Machinery
{
    /** @var  integer */
    private $id;
    /** @var  string */
    private $barcode;
    /** @var  integer */
    private $weigth;

    /**
     * Machinery constructor.
     * @param int $id
     * @param string $barcode
     * @param int $weigth
     */
    public function __construct($id, $barcode, $weigth)
    {
        $this->id = $id;
        $this->barcode = $barcode;
        $this->weigth = $weigth;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getBarcode(): string
    {
        return $this->barcode;
    }

    /**
     * @param string $barcode
     */
    public function setBarcode(string $barcode)
    {
        $this->barcode = $barcode;
    }

    /**
     * @return int
     */
    public function getWeigth(): int
    {
        return $this->weigth;
    }

    /**
     * @param int $weigth
     */
    public function setWeigth(int $weigth)
    {
        $this->weigth = $weigth;
    }


}