<?php

namespace Model;

class Parcel
{
    /** @var  integer */
    private $id;
    /** @var  integer */
    private $weight;
    /** @var  string  */
    private $barcode;
    /** @var  string */
    private $status;

    /**
     * Parcel constructor.
     * @param int $id
     * @param int $weight
     */
    public function __construct($id, $barcode, $weight)
    {
        $this->id = $id;
        $this->barcode = $barcode;
        $this->weight = $weight;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getWeight(): int
    {
        return $this->weight;
    }

    /**
     * @param int $weight
     */
    public function setWeight(int $weight)
    {
        $this->weight = $weight;
    }

    /**
     * @return string
     */
    public function getBarcode(): string
    {
        return $this->barcode;
    }

    /**
     * @param string $barcode
     */
    public function setBarcode(string $barcode)
    {
        $this->barcode = $barcode;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status)
    {
        $this->status = $status;
    }

}