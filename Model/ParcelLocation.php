<?php

namespace Model;


class ParcelLocation
{
    const SMALL_LOCALLY_TRACK = 1;
    const PALNE = 2;

    /** @var  integer */
    private $vehicleType;

    /**
     * @return int
     */
    public function getVehicleType(): int
    {
        return $this->vehicleType;
    }

    /**
     * @param int $vehicleType
     */
    public function setVehicleType(int $vehicleType)
    {
        $this->vehicleType = $vehicleType;
    }


}