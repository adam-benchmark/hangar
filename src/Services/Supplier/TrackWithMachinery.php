<?php

namespace Services\Supplier;


use ArrayObject;
use Model\Machinery;

class TrackWithMachinery implements BuilderSupplierInterface
{
    const PREFIX_FOR_MACHINERY = 'MACHINERY';
    const MIN_AMOUNT_OF_MACHINERY = 1;
    const MAX_AMOUNT_OF_MACHINERY = 2;
    const MIN_WEIGHT = 1500;
    const MAX_WEIGHT = 2000;

    /**
     * Generator Random Machinery for Test
     * @return array
     */
    public function buildSupplierDetails()
    {
        $machineries = new ArrayObject();

        for ($i = 1; $i <= $this->resolveRandomMachinariesAmount(); $i++) {
            $machinery = new Machinery($i, $this->resolveBarcode(), $this->randomWeight());
            $machineries->append($machinery);
        }

        return $machineries;
    }

    /**
     * Return Random Amount of Machinaries
     * @return int
     */
    private function resolveRandomMachinariesAmount()
    {
        return rand(self::MIN_AMOUNT_OF_MACHINERY, self::MAX_AMOUNT_OF_MACHINERY);
    }

    /**
     * Return Random Barcode
     * @return string
     */
    private function resolveBarcode()
    {
        return sprintf('%s%04s', self::PREFIX_FOR_MACHINERY, rand(1, 1000));
    }

    /**
     * Return Random Weight
     * @return integer
     */
    private function randomWeight()
    {
        $randomIndex = rand(self::MIN_AMOUNT_OF_MACHINERY - 1, self::MAX_AMOUNT_OF_MACHINERY - 1);
        $tabOfWeight = [
            self::MIN_WEIGHT,
            self::MAX_WEIGHT
        ];

        return $tabOfWeight[$randomIndex];
    }
}