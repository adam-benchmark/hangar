<?php

namespace Services\Supplier;

use ArrayObject;
use Model\Parcel;

class TrackWithParcel implements BuilderSupplierInterface
{
    const PREFIX_FOR_PARCEL = 'PARCEL';
    const MIN_PARCELS_PER_TRACK = 15;
    const MAX_PARCELS_PER_TRACK = 40;
    const PARCEL_MIN_WEIGHT = 10;
    const PARCEL_MAX_WEIGHT = 20;

    /**
     * @return ArrayObject
     */
    public function buildSupplierDetails()
    {
        $parcels = new ArrayObject();

        for ($i = 1; $i <= $this->resolveRandomParcelsAmount(); $i++) {
            $parcel = new Parcel($i, $this->resolveBarcode(), $this->resolveWeight());
            $parcels->append($parcel);
        }

        return $parcels;
    }

    /**
     * Return Random Amount of Parcels
     * @return int
     */
    private function resolveRandomParcelsAmount()
    {
        return rand(self::MIN_PARCELS_PER_TRACK, self::MAX_PARCELS_PER_TRACK);
    }

    /**
     * Return Random Barcode
     * @return string
     */
    private function resolveBarcode()
    {
        return sprintf('%s%04s', self::PREFIX_FOR_PARCEL, rand(1, 1000));
    }

    /**
     * Return Random Weight for Parcel
     * @return int
     */
    private function resolveWeight()
    {
        return rand(self::PARCEL_MIN_WEIGHT, self::PARCEL_MAX_WEIGHT);
    }
}