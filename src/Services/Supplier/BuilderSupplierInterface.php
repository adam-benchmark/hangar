<?php

namespace Services\Supplier;

interface BuilderSupplierInterface
{
    public function buildSupplierDetails();
}