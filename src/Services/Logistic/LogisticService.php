<?php

namespace Services\Logistic;

use ArrayObject;
use Model\Parcel;
use Model\SmallLocallyTrack;


class LogisticService extends LogisticBase
{
    const ERROR_PREFIX = 'Logistic Service | ';

    /**
     * LogisticService constructor.
     * @param ArrayObject $smallLocallyTracks
     */
    public function __construct(ArrayObject $smallLocallyTracks)
    {
        parent::__construct($smallLocallyTracks);
    }

    /**
     * @param $id
     * @return Parcel
     */
    public function findParcelById($id)
    {
        return $this->findParcelByIdFromBase($id);
    }

    /**
     * @param $id
     * @return SmallLocallyTrack
     */
    public function resolveTrackDetailsByParcelId($id)
    {
        return $this->resolveTrackDetailsByParcelIdFromBase($id);
    }
}