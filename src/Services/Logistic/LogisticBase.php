<?php

namespace Services\Logistic;

use ArrayObject;
use Model\Parcel;
use Model\SmallLocallyTrack;

abstract class LogisticBase
{
    /**
     * @var ArrayObject
     */
    private $smallLocallyTracks;

    /**
     * LogisticBase constructor.
     * @param ArrayObject $smallLocallyTracks
     */
    public function __construct(ArrayObject $smallLocallyTracks)
    {
        $this->smallLocallyTracks = $smallLocallyTracks;
    }

    /**
     * @return ArrayObject
     */
    public function getWholeSmallLocallyTracks()
    {
        return $this->smallLocallyTracks;
    }

    /**
     * @param ArrayObject $smallLocallyTracks
     * @param int $id
     * @return SmallLocallyTrack
     * @throws \Exception
     */
    protected function resolveTrackDetailsByParcelIdFromBase(int $id)
    {
        if ((int)$id == 0) {
            throw new \Exception(LogisticService::ERROR_PREFIX . 'wrong id for parcel');
        }

        /** @var SmallLocallyTrack $track */
        foreach ($this->smallLocallyTracks as $track) {
            /** @var Parcel $parcel */
            foreach ($track->getParcels() as $parcel) {
                if($parcel->getId() === $id) {
                    $trackTemporary = new SmallLocallyTrack();
                    $trackTemporary->setTrackBarcode($track->getTrackBarcode());
                    $trackTemporary->setTotalWeight($track->getTotalWeight());
                    $arrayObjectOfParcels = new ArrayObject();
                    $arrayObjectOfParcels->append($parcel);
                    $trackTemporary->setParcels($arrayObjectOfParcels);

                    return $trackTemporary;
                }
            }
        }

        throw new \Exception(LogisticService::ERROR_PREFIX . 'id: '. $id . ' for parcel not found!');
    }

    /**
     * @param $id
     * @return Parcel
     * @throws \Exception
     */
    public function findParcelByIdFromBase($id)
    {
        if ((int)$id == 0) {
            throw new \Exception(LogisticService::ERROR_PREFIX . 'wrong id');
        }

        /** @var SmallLocallyTrack $track */
        foreach ($this->smallLocallyTracks as $kTrack => $track) {
            /** @var Parcel $parcel */
            foreach ($track->getParcels() as $parcel) {
                if($parcel->getId() === $id) {

                    return $parcel;
                }
            }
        }

        throw new \Exception(LogisticService::ERROR_PREFIX . 'id: '. $id . ' not found');
    }
}