<?php

namespace Services\SplitToTrack;

use ArrayObject;
use Model\Parcel;
use Model\SmallLocallyTrack;

class SplitSmallParcelsToTrack
{
    const READY_TO_MOVE = 'ReadyToMoveToSmallLocallyTrack';


    /** @var  SmallLocallyTrack */
    private $smallLocallyTrack;

    /** @var  ArrayObject */
    private $smallLocallyTracks;

    /**
     * SplitToTrack constructor.
     */
    public function __construct()
    {

    }

    /**
     * @param ArrayObject $arrayObjectOfParcels
     * @param $maxWeigthForOneTrack
     * @return ArrayObject|SmallLocallyTrack[]
     */
    public function splitToTrack(ArrayObject $arrayObjectOfParcels, $maxWeigthForOneTrack): \ArrayObject
    {
        $this->smallLocallyTrack = new SmallLocallyTrack();
        $this->smallLocallyTracks = new ArrayObject();

        /**
         * @var Parcel $parcel
         */
        foreach ($arrayObjectOfParcels as $parcel) {
            if ($this->smallLocallyTrack->getTotalWeight() + $parcel->getWeight() <= $maxWeigthForOneTrack) {
                $this->smallLocallyTrack->addParcelAndIncreaseWeight($parcel);
                $parcel->setStatus(self::READY_TO_MOVE);
            } else {
                $this->smallLocallyTracks->append($this->smallLocallyTrack);
                $this->smallLocallyTrack = new SmallLocallyTrack();
                $this->smallLocallyTrack->addParcelAndIncreaseWeight($parcel);
                $parcel->setStatus(self::READY_TO_MOVE);
            }

        }

        $this->smallLocallyTracks->append($this->smallLocallyTrack);

        return $this->smallLocallyTracks;
    }

    /**
     * Get info about Tracks for Current Day
     * @return ArrayObject
     */
    public function getTracksDetails()
    {
        return $this->smallLocallyTracks;
    }

    /**
     * @param ArrayObject $arrayObjectOfParcels
     * @param $maxWeigthForOneTrack
     * @return float
     */
    public function resolveNumberOfTracks(ArrayObject $arrayObjectOfParcels, $maxWeigthForOneTrack)
    {
        $totalWeight = $this->calculateTotalWeight($arrayObjectOfParcels);
        $numberOfTracks = ceil($totalWeight / $maxWeigthForOneTrack);

        return $numberOfTracks;
    }

    /**
     * @param ArrayObject $arrayObjectOfParcels
     * @return int
     */
    private function calculateTotalWeight(ArrayObject $arrayObjectOfParcels)
    {
        $totalWeight = 0;
        foreach ($arrayObjectOfParcels as $parcel) {
            $totalWeight += $parcel->getWeight();
        }

        return $totalWeight;
    }
}