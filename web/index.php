<?php

use Model\SmallLocallyTrack;
use Services\Logistic\LogisticService;
use Services\SplitToTrack\SplitSmallParcelsToTrack;
use Services\Supplier\TrackWithMachinery;
use Services\Supplier\TrackWithParcel;

spl_autoload_register(function ($nazwa) {
    $pos = strripos($nazwa, '\\');
    $classNameWithoutNamespace = substr($nazwa, $pos + 1, strlen($nazwa));

    $paths = array(
        '../Model/' . $classNameWithoutNamespace . '.php',
        '../src/Services/Logistic/' . $classNameWithoutNamespace . '.php',
        '../src/Services/Supplier/' . $classNameWithoutNamespace . '.php',
        '../src/Services/SplitToTrack/' . $classNameWithoutNamespace . '.php',
    );
    foreach ($paths as $file) {
        if (file_exists($file)) {
            require($file);
        }
    }
});

echo '<p>welcome in hangar service</p>';
$trackWithParcel = new TrackWithParcel();
$parcels = $trackWithParcel->buildSupplierDetails();

echo '<p>-- random Parcels from Supplier --</p>';
echo '<pre>';
echo '<p>number of parcels: ' . $parcels->count() . '</p>';
print_r($parcels);
echo '</pre>';

$trackWithMachinery = new TrackWithMachinery();
$trackWithMachineries = $trackWithMachinery->buildSupplierDetails();

echo '<hr />';
echo '<p>-- random Machineries from Supplier --</p>';
echo '<pre>';
echo '<p>number of machineries: ' . $trackWithMachineries->count() . '</p>';
print_r($trackWithMachineries);
echo '</pre>';
echo '<hr />';
echo '<p>-- List of Splited Parcels for Logistic Guy --</p>';
$splitToSmallLocallyTrack = new SplitSmallParcelsToTrack();
$numberOfTrackForCurrentDay = $splitToSmallLocallyTrack->resolveNumberOfTracks($parcels, SmallLocallyTrack::SMALL_LOCALLY_TRACK_MAX_WEIGHT);
echo '<p>number of small locally track for current day: ' . $numberOfTrackForCurrentDay . '</p>';

$smallLocallyTracks = $splitToSmallLocallyTrack->splitToTrack($parcels, SmallLocallyTrack::SMALL_LOCALLY_TRACK_MAX_WEIGHT);

$logisticService = new LogisticService($smallLocallyTracks);

echo '<pre>';
//print_r($smallLocallyTracks);
//print_r($logisticService->getWholeSmallLocallyTracks());

try {
    $findParcel = $logisticService->findParcelById(0);
} catch (Exception $e) {
    print('error: ' . $e->getMessage());
}

echo '<p>Find parcel by id </p>';
try {
    $findParcel = $logisticService->findParcelById(15);
    print_r($findParcel);
} catch (Exception $e) {
    print('error: ' . $e->getMessage());
}


/*echo '<hr />';
echo '<p>-- show whole tracks with parcels -- </p>';
echo '<pre>';
print_r($logisticService->getWholeSmallLocallyTracks());
echo '</pre>';*/
echo '<hr />';
echo '<p> -- show Track via Parcel id -- </p>';
try {
    $result = $logisticService->resolveTrackDetailsByParcelId( 16);
    print_r($result);
} catch (Exception $exception) {
    print('error: ' . $exception->getMessage());
}
echo '<p>-- Game over --</p>';
